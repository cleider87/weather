# Weather ![build](https://gitlab.com/cleider87/weather/badges/master/build.svg?job=build)

## Instalación

```sh
# Instalación
npm install mocha -g
npm install istanbul -g
npm install concurrently -g
npm install pm2 -g

npm install && cd cd client && npm install && cd ..
# Ejecución de pruebas unitarias
npm test

# Desplegar la aplicación para desarrollo
npm run dev

# Desplegar la aplicación para producción
npm run prod
```

## API WEATHER

Se debe fijar como variable de entorno "WEATHER_APIKEY" para el acceso del servicio de openweathermap.org
o se reescribe el archivo config/config.js

### API Versión

GET /v1/

Muestra los datos de la api actual

```javascript 
{
	"api": "weather",
	"version": "1.0.0"
}
```

### Location

Muestra la localización actual de la IP

```javascript
// Servicios probados
// 'https://ipapi.co/json'
// 'http://ip-api.com/json'
// 'http://ip-api.io/json'

```

```
GET /v1/location/
```

```javascript 
{
	"ip": "181.XX.XXX.XX",
	"city": "Buenos Aires",
	"region": "Ciudad Autónoma De Buenos Aires",
	"region_code": null,
	"country": "AR",
	"country_name": "Argentina",
	"continent_code": "SA",
	"in_eu": false,
	"postal": null,
	"latitude": -34.603684,
	"longitude": -58.381559,
	"timezone": null,
	"utc_offset": null,
	"country_calling_code": "+54",
	"currency": "ARS",
	"languages": "es-AR,en,it,de,fr,gn",
	"asn": "AS27747",
	"org": "Telecentro S.A."
}
```

### Current

Muestra el clima de la ciudad consultada o de la ciudad actual
Servicios de Geolocalización probados

```
GET /v1/forecast/:city
```

```javascript 
{
	"coord": {
		"lon": -58.44,
		"lat": -34.61
	},
	"weather": [
		{
			"id": 800,
			"main": "Clear",
			"description": "clear sky",
			"icon": "01n"
		}
	],
	"base": "stations",
	"main": {
		"temp": 8.93,
		"pressure": 1017,
		"humidity": 76,
		"temp_min": 8,
		"temp_max": 10
	},
	"visibility": 10000,
	"wind": {
		"speed": 3.1,
		"deg": 320
	},
	"clouds": {
		"all": 0
	},
	"dt": 1532224800,
	"sys": {
		"type": 1,
		"id": 4685,
		"message": 0.0031,
		"country": "AR",
		"sunrise": 1532256878,
		"sunset": 1532293567
	},
	"id": 3435910,
	"name": "Buenos Aires",
	"cod": 200
}
```

### Forecast

Muestra el pronóstico de la ciudad consultada o de la ciudad actual

```
GET /v1/forecast/:city
```

```javascript 
{
	"cod": "200",
	"message": 0.0049,
	"cnt": 39,
	"list": [
		{
			"dt": 1532228400,
			"main": {
				"temp": 9.05,
				"temp_min": 7.44,
				"temp_max": 9.05,
				"pressure": 1030,
				"sea_level": 1031.6,
				"grnd_level": 1030,
				"humidity": 89,
				"temp_kf": 1.61
			},
			"weather": [
				{
					"id": 800,
					"main": "Clear",
					"description": "clear sky",
					"icon": "01n"
				}
			],
			"clouds": {
				"all": 0
			},
			"wind": {
				"speed": 5.76,
				"deg": 352.501
			},
			"sys": {
				"pod": "n"
			},
			"dt_txt": "2018-07-22 03:00:00"
		},
		{...}
	],
	"city": {
		"id": 3435910,
		"name": "Buenos Aires",
		"coord": {
			"lat": -34.6076,
			"lon": -58.4371
		},
		"country": "AR",
		"population": 1000000
	}
}
```