module.exports = {
    PORT: process.env.PORT || 8080,
    wheather_apikey: process.env.WEATHER_APIKEY,
    ip_api_url: 'http://ipapi.co/json',
    wheather_current: 'https://api.openweathermap.org/data/2.5/weather',
    wheather_forecast: 'https://api.openweathermap.org/data/2.5/forecast'
}