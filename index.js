const server = require('./server')

const config = require('./config/config')

const api = server.listen(config.PORT, function () {
    console.log("API running on port ", api.address().port);
});