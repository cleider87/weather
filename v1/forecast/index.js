const express = require("express")

const router = express.Router()

const request = require('request-promise')

const config = require('../../config/config')

const queryConfig = '?mode=json&units=metric&appid='+config.wheather_apikey+'&q='

router.get('/', (req, res, next) => {
    const options = {
        method: 'GET',
        uri: config.ip_api_url,
        resolveWithFullResponse: true,
        json: true
    }

    request(options)
    .then(ipRes => {
        request(config.wheather_forecast +queryConfig+ipRes.body.city)
        .then(response => {
            res.setHeader('Content-Type', 'application/json');
            res.status(200).send(response)
            next()
        })
        .catch(err => {
            res.status(404).send({ message:'Forecast Weather not found'});
            next()
        })
    })
    .catch(err => {
        res.status(404).send({message: 'IP Location not found'});
        next()
    })
})

router.get('/:city', (req, res, next) => {
    request(config.wheather_forecast +queryConfig+req.params.city)
    .then(response => {
        res.setHeader('Content-Type', 'application/json');
        res.status(200).send(response)
        next()
    })
    .catch(err => {
        res.status(404).send({ message:'Forecast Weather not found'});
        next()
    })
})

module.exports = router;