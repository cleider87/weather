const express = require("express")

const location = require('./location')

const current = require('./current')

const forecast = require('./forecast')

const router = express.Router()

router.get('/', (req, res, next) => {
    res.setHeader('Content-Type', 'application/json');
    res.send({
        'api': 'weather',
        'version': '1.0.0'
    })
    next()
})

router.use('/location', location)

router.use('/current', current)

router.use('/forecast', forecast)

module.exports = router;