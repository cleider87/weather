const express = require("express")

const router = express.Router()

const request = require('request-promise')

const config = require('../../config/config')

router.get('/', (req, res, next) => {
    
    const options = {
        method: 'GET',
        uri: config.ip_api_url,
        resolveWithFullResponse: true,
        json: true
    }

    request(options)
    .then(response => {
        res.send(response)
        next()
    })
    .catch(err => {
        console.log(err)
        next()
    })
})



module.exports = router;