import React, { Component } from 'react';

import { Container, Row, Col } from 'reactstrap';

import CityCard from './componets/CityCard';

import logo from './icon.png';

class App extends Component {
  constructor(props) {
    super(props)

    this.state={
      cities: [
        'Caracas',
        'Chicago',
        'Tokio',
        'Bogota'
      ]
    }
    
  }

  componentDidMount() {
    
  }

  _renderCurrentCity(city='') {
    return (<CityCard queryCity={city} />)
  }

  _renderCities(cities) {
    return (
      <Row>
        {
          this.state.cities.map(city=>{
              return <Col sm="6" lg="6"> {this._renderCurrentCity(city)}</Col>
          })
        }
      </Row>
    )
  }

  render() {
    return (
      <Container>
        <Row>
          <Col lg="2">
            <img src={logo} className="" width="100px"/>
          </Col>
          <Col lg="10">
            <br/>
            <h1>Weather</h1>
          </Col>
        </Row>
        <Row>
          <Col sm="6" lg="4">
            {this._renderCurrentCity()}
          </Col>
          <Col sm="6" lg="6">
            {this._renderCities()}
          </Col>
        </Row>
      </Container>
    );
  }
}

export default App;