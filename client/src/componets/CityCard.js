import React, { Component } from 'react';

import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle } from 'reactstrap';

import 'bootstrap/dist/css/bootstrap.min.css';

class CityCard extends Component {
  constructor(props) {
    super(props)

    this.state = {
      currentCity: {
        city: 'Ciudad',
        country_name: 'País',
      },
      currentWeather: {
        main: {
          temp: 0,
          temp_min: 0,
          temp_max: 0
        }
      },
      currentForecast: {},
      icon: 'http://openweathermap.org/img/w/01d.png'
    }
  }

  componentDidMount(prevProps, prevState) {
    this.getLocation()
      .then(res => this.setState({ currentCity: res.body }))
      .catch(err => console.log(err));

    this.getWeather(this.props.queryCity)
      .then(res => {
        const weather = res;
        const icon = 'http://openweathermap.org/img/w/' + weather.weather[0].icon + '.png';

        this.setState({
          currentWeather: weather,
          icon: icon
        })
      })
      .catch(err => console.log(err));
  }

  componentDidUpdate(prevProps, prevState) {
    console.log(this.state)
  }

  getLocation = async () => {
    const response = await fetch('/v1/location');
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  getWeather = async ( city = '' ) => {
    const response = await fetch('/v1/current/' + city);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  getForecast = async ( city = '' ) => {
    const response = await fetch('/v1/forecast/' + city);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  render() {
    return (
      <div>
        <Card>
          <CardImg top width="50px" src={this.state.icon} alt="Current City" />
          <CardBody>
            <CardTitle> {this.state.currentWeather.name}</CardTitle>
            <CardSubtitle>{this.state.currentWeather.main.temp} ° C</CardSubtitle>
            <CardText>
              Max: {this.state.currentWeather.main.temp_max} ° C<br />
              Min: {this.state.currentWeather.main.temp_min} ° C<br />
            </CardText>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default CityCard;
