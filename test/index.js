const request = require('supertest')

const should = require('should')

const app = require('../server')

describe('GET /v1', _ => {

  it('should api v1 json ok', done => {
    request(app)
      .get('/v1')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) done(err)
        done()
      })
  })

  it('should api v1 response ok', done => {
    request(app)
      .get('/v1')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) done(err)
        res.body.should.have.properties(['version','api'])
        done()
      })
  })

})

describe('GET /v1/location', _ => {
  it('should be get this location city', done => {
    request(app)
      .get('/v1/location')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) done(err)
        res.body.body.should.have.any.keys('city')
        done()
      })
  })
})

describe('GET /v1/current', _ => {

  it('should be get this weather current', done => {
    request(app)
      .get('/v1/current')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)    
      .end((err, res) => {
        if (err) done(err)
        res.body.should.have.keys('weather')
        done()
      })
  })

  it('should be with json weather current from zootopia not found', done => {
    request(app)
      .get('/v1/current/zootopia')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(404)
      .end((err, res) => {
        if (err) done(err)
        res.body.message.should.be.equal('Weather Current not found')
        done()
      })
  })

})


describe('GET /v1/forecast', _ => {

  it('should be get this forecast weather', done => {
    request(app)
      .get('/v1/forecast')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)    
      .end((err, res) => {
        if (err) done(err)
        res.body.should.have.keys('list')
        done()
      })
  })

  it('should be with json forecast weathers zootopia not found', done => {
    request(app)
      .get('/v1/forecast/zootopia')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(404)
      .end((err, res) => {
        if (err) done(err)
        res.body.message.should.be.equal('Forecast Weather not found')
        done()
      })
  })
  
})