const express = require("express")

const app = express()

const bodyParser = require('body-parser');

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));

const v1 = require('./v1')

app.use('/v1', v1)


module.exports = app;